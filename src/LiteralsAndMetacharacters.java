import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LiteralsAndMetacharacters {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile("^[a-e]");
        Pattern pattern2 = Pattern.compile("[a-e]$");
        Pattern pattern3 = Pattern.compile(".[0-9]");
        Pattern pattern4 = Pattern.compile(".[0-9]|.[a-c]");
        Pattern pattern5 = Pattern.compile("\\d"); //цифровой символ
        Pattern pattern6 = Pattern.compile("\\D"); //не цифровой символ
        Pattern pattern7 = Pattern.compile("\\s"); //поиск пробела
        Pattern pattern8 = Pattern.compile("\\f\\n\\r\\t\\v"); //альтернатива //s
        Pattern pattern9 = Pattern.compile("\\S"); //поиск не пробельных символов

        Matcher matcher1 = pattern1.matcher("a b c d e f g h");
        System.out.println(matcher1.find());

        Matcher matcher2 = pattern1.matcher("f g h a b c");

        System.out.println(matcher2.find());

        Matcher matcher3 = pattern1.matcher("1 2 3");
        System.out.println(matcher3.find());
    }
}
