import java.util.regex.Pattern;

public class Quantifiers {
    public static void main(String[] args) {
        System.out.println(Pattern.compile("10{2}").matcher("100").find());
        System.out.println(Pattern.compile("10{1,3}").matcher("100").find());
        System.out.println(Pattern.compile("10{2,}").matcher("100").find());
        System.out.println(Pattern.compile("10?").matcher("100").find());
        System.out.println(Pattern.compile("10*").matcher("100").find());
        System.out.println(Pattern.compile("10+").matcher("100").find());
    }
}
