import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class regularExpressionGrouping {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(\\d+).*\\1");

        Matcher matcher = pattern.matcher("2016 year. 2018 year. 2018 year.");

        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }

    }
}
